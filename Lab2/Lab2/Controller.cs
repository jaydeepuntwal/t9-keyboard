﻿/*
 * Controller.cs
 * Controller for T9 keyboard
 * @author Jaydeep Untwal
 * Rochester Institute of Technology
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Controller
    {

        private Model m;
        // Current Input Code
        private string input = "";
        // Last Predicted Word
        private string tempPred = "";

        /// <summary>
        /// Constructor
        /// </summary>
        public Controller()
        {
            m = new Model();
        }

        /// <summary>
        /// Reset Input and Prediction
        /// </summary>
        public void resetStrings()
        {
            input = "";
            tempPred = "";
        }

        /// <summary>
        /// Toggle Predictive Mode
        /// </summary>
        public void togPredMode()
        {
            m.togglePred();
            resetStrings();
        }

        /// <summary>
        /// Get Content for Display
        /// </summary>
        /// <param name="CClicks">No of clicks</param>
        /// <param name="Name">Name of Button who called</param>
        /// <param name="CContent">Current Content</param>
        /// <param name="ToolTip">ToolTip of Button</param>
        /// <returns>Content to display on screen</returns>
        public string getContent(int CClicks, string Name, string CContent, string ToolTip)
        {
            // Predictive Mode
            if (m.getPredMode())
            {

                if (Name == "Space")
                {
                    string temp = CContent;
                    temp += " ";
                    resetStrings();               
                    return temp;
                }
                else if (Name == "Star")
                {

                    if (CContent.Length > 0)
                    {

                        char[] tempCon = CContent.ToCharArray();
                        int tempConPos = CContent.Length - 1;
                        string temp = "";
                        string[] parts = CContent.Split(' ');

                        // Remove whole word
                        if (tempCon[tempConPos] == ' ')
                        {
                            for (int i = 0; i < parts.Length - 2; i++)
                            {
                                temp += parts[i];
                                temp += " ";
                            }

                            if (temp.Length == 1)
                            {
                                temp = "";
                            }

                        }

                        // Remove one character and return last prediction
                        else
                        {
                            temp = CContent;

                            if (input == "" && tempPred == "")
                            {
                                if (temp.Length > 0)
                                {
                                    return temp.Substring(0, temp.Length - 1);
                                }
                                else
                                {
                                    return temp;
                                }
                            }

                            if (input.Length > 0)
                                input = input.Remove(input.Length - 1);

                            if (temp.Length > 0)
                                temp = temp.Substring(0, temp.Length - tempPred.Length);

                            m.getWords(input);
                            tempPred = m.getPrediction();
                            if(input.Length > 0)
                                temp += tempPred;

                        }

                        return temp;

                    }

                }
                else if (Name == "One")
                {
                    return CContent;
                }
                else if (Name == "Next")
                {
                    // Return next prediction
                    if (input != "")
                    {
                        string[] parts = CContent.Split(' ');
                        string temp = CContent.Substring(0, CContent.Length - parts[parts.Length - 1].Length);
                        tempPred = m.getNextPrediction();
                        return temp += tempPred;
                    }
                }
                else
                {
                    // Get Prediction from Model and return string
                    input += ToolTip;
                    m.getWords(input);
                    string[] parts = CContent.Split(' ');
                    string temp = CContent.Substring(0, CContent.Length - parts[parts.Length - 1].Length);
                    tempPred = m.getPrediction();
                    return temp += tempPred;   
                }
            }

            // Non - Predictive Mode
            else
            {
                if (Name == "Space")
                {
                    string temp = CContent;
                    temp += " ";
                    resetStrings();
                    return temp;
                }
                else if (Name == "Next")
                {
                    return CContent;
                }
                else if (Name == "Star")
                {
                    if (CContent.Length > 0)
                    {
                        return CContent.Substring(0, CContent.Length - 1);
                    }

                }
                else if (Name == "One")
                {
                    return CContent;
                }
                else
                {
                    
                // Return character based on number of clicks
                    if (Name == "pqrs" || Name == "wxyz")
                    {
                        if (CClicks > 4)
                        {
                            CClicks = 1;
                        }
                    }
                    else
                    {
                        if (CClicks > 3)
                        {
                            CClicks = 1;
                        }
                    }


                    if (CClicks == 1)
                    {
                        return CContent += Name.ToCharArray()[CClicks - 1];
                    }
                    else if (CClicks >= 2)
                    {
                        string temp = CContent.Substring(0, CContent.Length - 1);
                        return temp += Name.ToCharArray()[CClicks - 1];
                    }

                }
            }

            return CContent;
        }

    }
}
