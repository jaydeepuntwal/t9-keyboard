﻿/*
 * MainWindow.xaml.cs
 * View for T9 keyboard
 * @author Jaydeep Untwal
 * Rochester Institute of Technology
 * 
 * How to Use:
 * 
 * You can enter text in Predictive and Non Predictive Mode
 * 
 * Button * , < : Backspace
 * Button # : Space
 * Button 0, ~ : Next Prediction (In Predictive Mode)
 * 
 * Predictive mode:
 * To turn predictive mode on, click on the predictive mode button
 * Just punch in the numbers related to the alphabet
 * Eg. FOOD - 3663
 * If some other word is displayed, click on '0' to get next prediction
 * 
 * NonPredictive Mode:
 * Just punch in the number related to the alphabet
 * Eg. For c - Click 2 three times
 *     For z - Click 9 four times
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Object of Controller
        Controller c;

        /// <summary>
        /// Constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            c = new Controller();
            predModeBtn.Content = "Predictive Mode OFF";
            predModeBtn.BorderThickness = new Thickness(1);
        }

        /// <summary>
        /// If Predictive Mode Button Pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void predModeBtn_Checked(object sender, RoutedEventArgs e)
        {
            c.togPredMode();
            predModeBtn.BorderThickness = new Thickness(4);
            predModeBtn.Content = "Predictive Mode ON";
            c.resetStrings();
        }

        /// <summary>
        /// If Predictive Mode Button Released
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void predModeBtn_Unchecked(object sender, RoutedEventArgs e)
        {
            c.togPredMode();
            predModeBtn.BorderThickness = new Thickness(1);
            predModeBtn.Content = "Predictive Mode OFF";
            c.resetStrings();
        }

        /// <summary>
        /// Numpad Button Press
        /// </summary>
        /// <param name="sender">Button</param>
        /// <param name="e">MouseEvent</param>
        private void KeyPad_Click(object sender, MouseButtonEventArgs e)
        {
            FrameworkElement feSource = e.Source as FrameworkElement;
            // Caller Name
            string name = feSource.Name;
            // Caller Value
            string ttip = (string)feSource.ToolTip;
            // Get content from Controller
            String content = c.getContent(e.ClickCount, name, wordsTxt.Text, ttip);
            wordsTxt.Text = content;
        }

    }
}
