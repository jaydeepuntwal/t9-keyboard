﻿/*
 * Model.cs
 * Class to store the data required for T9 keyboard
 * @author Jaydeep Untwal
 * Rochester Institute of Technology
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Model

    {
        private Dictionary<string,string> words; // Dictionary
        private ArrayList prediction; // Predicted words
        private int predIndex; // Current prediction index
        private bool predMode; // Current T9 Mode
        private string lastPrediction = " "; // Last Prediction

        /// <summary>
        /// Constructor of Model
        /// </summary>
        public Model()
        {
            predMode = false;
            words = new Dictionary<string,string>();
            storeWords();
        }

        /// <summary>
        /// Getter for Prediction Mode
        /// </summary>
        /// <returns>true if prediction mode is on else false</returns>
        public bool getPredMode()
        {
            return predMode;
        }

        /// <summary>
        /// Encoding the Character
        /// </summary>
        /// <param name="a">Character</param>
        /// <returns>Code</returns>
        private int getCode(char a)
        {
            if (a == 'a' || a == 'b' || a == 'c')
            {
                return 2;
            }
            else if (a == 'd' || a == 'e' || a == 'f')
            {
                return 3;
            }
            else if (a == 'g' || a == 'h' || a == 'i')
            {
                return 4;
            }
            else if (a == 'j' || a == 'k' || a == 'l')
            {
                return 5;
            }
            else if (a == 'm' || a == 'n' || a == 'o')
            {
                return 6;
            }
            else if (a == 'p' || a == 'q' || a == 'r' || a == 's')
            {
                return 7;
            }
            else if (a == 't' || a == 'u' || a == 'v')
            {
                return 8;
            }
            else if (a == 'w' || a == 'x' || a == 'y' || a == 'z')
            {
                return 9;
            }

            return 0;
        }

        /// <summary>
        /// Storing words in dictionary
        /// </summary>
        public void storeWords()
        {
            string line;
            StreamReader file = new StreamReader(@"C:\english-words.txt");
            while ((line = file.ReadLine()) != null)
            {
                string code = "";

                for (int i = 0; i < line.Length; i++)
                {
                    // Encode each character
                    code += getCode(line.ToCharArray()[i]);
                }

                words.Add(line,code);
            }

            file.Close();
        }

        /// <summary>
        /// Get Words for current input
        /// </summary>
        /// <param name="input">Input Code</param>
        public void getWords(string input)
        {

            if (input == "")
            {
                predIndex = prediction.Count;
                return;
            }

            prediction = new ArrayList();

            // Get Words by length and prefix using LINQ
            var w =
            from results in words
            where results.Value.StartsWith(input) && results.Value.Length == input.Length
            select results.Key;

            // If no words available for length
            // Get words by prefix only
            if (w.Count() == 0)
            {
                w =
                from results in words
                where results.Value.StartsWith(input) && results.Value.Length >= input.Length
                select results.Key;
            }

            foreach (string word in w)
            {
                prediction.Add(word);
            }

            predIndex = 0;

        }

        /// <summary>
        /// Get First Prediction
        /// </summary>
        /// <returns>Predicted word</returns>
        public string getPrediction()
        {
            if (predIndex < prediction.Count)
            {
                lastPrediction = (string) prediction[0];
                return (string)prediction[0];
            }
            else
            {
                string temp = "";

                for (int i = 0; i < lastPrediction.Length; i++)
                {
                    temp += "-";
                }

                temp += "-";

                lastPrediction = temp;

                return temp;
            }
        }

        /// <summary>
        /// Get Predictions
        /// </summary>
        /// <returns>Each Predicted word</returns>
        public string getNextPrediction()
        {
            predIndex++;

            if (prediction != null && predIndex < prediction.Count)
            {
                lastPrediction = (string)prediction[predIndex];
                return (string)prediction[predIndex];
            }
            else
            {
                string temp = "";

                for (int i = 0; i < lastPrediction.Length; i++)
                {
                    temp += "-";
                }

                temp += "-";

                lastPrediction = temp;

                return temp;
            }
        }

        /// <summary>
        /// Get the total number of words
        /// </summary>
        /// <returns>Total Number of Words</returns>
        public int size()
        {
            return words.Count();
        }

        /// <summary>
        /// Toggle Predictive Mode
        /// </summary>
        public void togglePred()
        {
            if (predMode)
            {
                predMode = false;
            }
            else
            {
                predMode = true;
            }
        }
    }
}
